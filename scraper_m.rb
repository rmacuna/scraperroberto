require 'watir'
require 'phantomjs'
require_relative 'view'
require 'highline/import'
require 'mechanize'
require_relative 'models'


module Scrapper
    def self.login usuario
        WatirModule.clickToLogin
        sleep(2)
        WatirModule.sendKeys usuario.sid, usuario.pass
        sleep(3)
        isLogin = validation
        return isLogin
    end

    # Valida si las credenciales son correctas
    def self.validation
        if WatirModule.exists("settingsIcon")
            return true
        else
            return false
        end
    end

    def self.bookSpecificRoom 
        WatirModule.goToBookSpecificRoom
        sleep(1)
        robot = Watir.createBot
        table = robot.css('.ClickableRow')
        return table
    end

    def self.getDropDownsValues
        WatirModule.goToFindARoom
        durationList = WatirModule.getDropdownOptions("//*[@id='cboDuration']")
        beginHourList= WatirModule.getDropdownOptions("//*[@id='cboStartTime']")
        endHourList = WatirModule.getDropdownOptions("//*[@id='cboEndTime']")
        optionsOf = [durationList,beginHourList,endHourList]
        return optionsOf
    end

    def self.findARoom selectedValues
        WatirModule.dropdownClickList("cboLocation")
        WatirModule.dropdownSelectItem("//*[@id='cboLocation']/option[3]")

        duration = selectedValues[0]
        beginHour = selectedValues[1]
        endHours = selectedValues[2]
        sleep(1)

        WatirModule.dropdownSelectItem("//*[@id='cboDuration']/option[#{duration}]")
        WatirModule.dropdownSelectItem("//*[@id='cboStartTime']/option[#{beginHour}]")
        WatirModule.dropdownSelectItem("//*[@id='cboEndTime']/option[#{endHours}]")
        WatirModule.clickOnInput("//*[@id='btnAnyDate']")
        sleep(3)

        bot = WatirModule.createBot
        month1name = bot.css('#availabilityCalendar0 > table > tbody > tr:nth-child(1) > td > b').text
        month2name = bot.css('#availabilityCalendar1 > table > tbody > tr:nth-child(1) > td > b').text
        month3name = bot.css('#availabilityCalendar2 > table > tbody > tr:nth-child(1) > td > b').text

        avaliableMonths = [month1name,month2name,month3name]
        return avaliableMonths
    end

    def self.getCubsTable selectedMonth, day
        case selectedMonth
        when 1 then 
                    table = WatirModule.getTable "//*[@id='availabilityCalendar0']/table"
                    trs = WatirModule.getTrsOf table
             
                    trs.drop(2).each do |tds|      
                        tds.each do |td|
                        text = td.text.gsub(/\s+/, "") #Le quito los espacios al string 
                            if text =~ /\d/ 
                                if text.to_i == day
                                td.click
                                break
                                end
                            end
                        end
                    end

                     sleep(2)
                     bot = WatirModule.createBot
                     #Le devuelvo la tabla si es un dia valido sino le devuelvo nil
                    if WatirModule.cantBook
                         return nil
                    else
                        avaliable_cubs_table = bot.css('.ClickableRow')
                        return avaliable_cubs_table
                    end

            when 2 then
                    table = WatirModule.getTable "//*[@id='availabilityCalendar1']/table"
                    trs = WatirModule.getTrsOf table
             
                    trs.drop(2).each do |tds|      
                         tds.each do |td|
                         text = td.text.gsub(/\s+/, "")
                             if text =~ /\d/ 
                                 if text.to_i == day
                                    puts td.text
                                    td.click
                                    break
                                 end
                             end
                        end
                    end

                    sleep(2)
                    bot = WatirModule.createBot
                    if WatirModule.cantBook
                         return nil
                    else
                        avaliable_cubs_table = bot.css('.ClickableRow')
                        return avaliable_cubs_table
                    end

                when 3 then
                     table = WatirModule.getTable "//*[@id='availabilityCalendar2']/table"
                     trs = WatirModule.getTrsOf table
             
                     trs.drop(2).each do |tds|      
                              tds.each do |td|
                                text = td.text.gsub(/\s+/, "") #Le quito los espacios al string 
                                     if text =~ /\d/ 
                                       if text.to_i == day
                                        td.click
                                       break
                                    end
                                 end
                             end
                         end
                    sleep(2)
                    bot = WatirModule.createBot
                    if WatirModule.cantBook
                         return nil
                    else
                        avaliable_cubs_table = bot.css('.ClickableRow')
                        return avaliable_cubs_table
                    end
        end
    end

    def self.bookTheRoom selectedCub
            rows = WatirModule.getTheRows
            i = 0
            rows.each do |x| 
                x.td.click if i == selectedCub
                i = i+1
            end
            WatirModule.confirmBooking
            sleep (1)
    end
end





