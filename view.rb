
require 'highline/import'
require_relative 'scraper_m'
require_relative 'models'
require_relative 'watir_manager'




module View

     def self.startScrapper
     	puts '--'*20
     	puts '|' + ' '*10 + 'Scrapper_Roberto' +'                      |'
          puts '--'*20
     	WatirModule.init
     end

	def self.fillCredentials
		system("clear")
		print 'Ingrese su usuario uninorte: '
		sid = gets.chomp
		pass = password_prompt('Ingrese su contraseña: ')
          usuario = Usuario.new sid, pass
		cred = Scrapper.login usuario

    	if !cred
		puts 'Wrong credentials'
		sleep(1)
    		fillCredentials()
    	else
    		enteredRoom()
    	end

    end

	def self.enteredRoom
	 puts 'Ingresando a su cuenta.....'
     sleep(1)
     system "clear"       
     puts "Estas adentro"
     puts '|'+'---'*10 +      '|'
     puts '|'+'Finding a room' + '          (1)   |'
     puts '|'+'Book a specific room' + '    (2)   |'
     puts '|'+'---'*10 +      '|'
     print "¿Que desea hacer? : " 
     ans =  gets.chomp.to_i

     	if 1 == ans
     		puts 'Finding a Room....'
               WatirModule.goToFindARoom
     		dropdownLists = Scrapper.getDropDownsValues

               durations = dropdownToArray(dropdownLists[0])
     		beginHours = dropdownToArray(dropdownLists[1])
     		endHours = dropdownToArray(dropdownLists[2])

     		ans = 'n'
     		while ans == 'n'
     			puts 'Seleccione cuanto tiempo va a durar en el cubiculo'
     			value1 = eachList(durations, 1)
                    duration = durations[value1-1]
     			system("clear")

     			puts 'Seleccione desde que hora va a estar en el cubiculo' 
     			value2 = eachList(beginHours, 1)
                    beginHour = beginHours[value2-1]

     			system("clear")

     			puts 'Seleccione hasta que hora va a estar en el cubiculo'
     			value3 = eachList(endHours, 1)
                    endHour = endHours[value3-1]

     			system("clear")

     			puts 'Seleccionado: '
     			print "Duracion: #{duration}  Hora Inicial: #{beginHour}   Hora Final: #{endHour} "
     			puts 'Confirmar y / n ?'
                    
              
     			nextStep = gets.chomp.to_s
                    if nextStep == "y"
                           selectedValues = [value1, value2, value3]
                           avaliableMonths = Scrapper.findARoom(selectedValues)
                           break
                    end  
               end
  
               finished = false
                 while finished == false
                    system("clear")
                         puts 'Meses disponibles para reservar'
                         avaliableMonths.each { |value| puts value}
                         puts "#{avaliableMonths[0]} => (1) : #{avaliableMonths[1]} => (2) : #{avaliableMonths[2]} => (3) : nextMonths => (4)" 
                         selection = gets.chomp.to_i

                         while selection > 4 || selection < 1 
                         puts 'Ingrese un valor valido'
                         selection = gets.chomp.to_i
                         end

                               if selection == 4
                                   WatirModule.nextMonth
                                   sleep (3)
                                   month1 = WatirModule.getMonthName "//*[@id='availabilityCalendar0']/table/tbody/tr[1]/td/b"
                                   month2 = WatirModule.getMonthName "//*[@id='availabilityCalendar1']/table/tbody/tr[1]/td/b"
                                   month3 = WatirModule.getMonthName "//*[@id='availabilityCalendar2']/table/tbody/tr[1]/td/b"
                                   avaliableMonths.clear
                                   avaliableMonths = [month1,month2,month3]
                              else
                                   break
                              end
                         end

                    canBook = false
                    while canBook == false

                              puts 'Escoja un dia del 1 al 30'
                              day = gets.chomp.to_i

                                while day > 30 || day < 1
                                  puts 'Escoja un dia valido'
                                  day = gets.chomp.to_i
                                end

                           avaliable_cubs_table = Scrapper.getCubsTable selection, day

                           if avaliable_cubs_table != nil
                               i = 1
                              cubs = Array.new
                               avaliable_cubs_table.each do |item|
                                    puts "#{i}  #{item.css("td")[2].text}"
                                     i = i+1
                                    cubs.push item.css("td")[2].text
                               end

                          puts 'Seleccione el cubiculo que desea reservar'
                          selectedCub = gets.chomp.to_i
                          Scrapper.bookTheRoom selectedCub
                          break

                          else
                              puts 'No es posible reservar en este dia porque no hay cubiculos disponibles volver a intentar'
                              system("clear")
                          end

                    end

          elsif 2 == ans
               puts 'Booking specific Room'
               WatirModule.goToBookSpecificRoom
               sleep(1)
               rows = Scrapper.bookSpecificRoom

               rows.each do |x| 
                    puts x.css("td")[2].text
               end
     	end
	end

	def self.eachList(list, i)
		 	list.each do |item|
				puts "#{i}:  #{item} "
				!i = i + 1
     		end
     	return gets.chomp.to_i
	end

     def self.dropdownToArray dropdown 
          options = Array.new
          dropdown.each do |x|
               options.push(x.text)
          end
          return options
     end

	def self.password_prompt(message, mask='*')
 	  ask(message) { |q| q.echo = mask}
	end

	




end

