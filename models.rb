

class Usuario
	attr_accessor :sid, :pass
	# Attributes, #arguments
	def initialize (sid,pass)
		@sid = sid
		@pass = pass
	end
end

class Cubiculo
	attr_accessor :name, :avaliable, :type

	def initialize (name, avaliable, type)
		@avaliable = avaliable
		@type = type
		@name = name
	end
end