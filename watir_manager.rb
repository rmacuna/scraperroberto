require 'watir'
require 'phantomjs'

module WatirModule


	def self.init
		@browser = Watir::Browser.new(:phantomjs)
 		@browser.goto('http://guaymaro.uninorte.edu.co/UNEspacio/index.php?p=Index')
	end

	# Paremtro id
	def self.dropdownClickList item
		@browser.select_list(:id, "#{item}").click
	end

	def self.dropdownSelectItem xpath
		@browser.option(:xpath, "#{xpath}").click
	end

	def self.getDropdownOptions xpath
		return @browser.select_list(:xpath, "#{xpath}").options
	end

	def self.createBot
		return Nokogiri::HTML.parse(@browser.html)
	end

	def self.goToFindARoom
   		 @browser.a(:xpath, "//*[@id='menuContent']/ul/li[2]/ul/li[2]/div/a").click
	end

	def self.goToBookSpecificRoom 
		@browser.a(:xpath, "//*[@id='menuContent']/ul/li[2]/ul/li[3]/div/a").click
	end

	def self.clickToLogin 
		@browser.div(:id => "portalMenuBar").li(:text => /Log in/).click
	end

	def self.clickOnInput xpath
		@browser.input(:xpath,  "#{xpath}").click
	end

	def self.sendKeys sid, pass
		@browser.input(:name => 'txtUserName').select
    	@browser.send_keys sid
   		@browser.input(:name => 'txtPassword').click
    	@browser.send_keys pass
    	@browser.input(:id => 'btnLogin').click
	end

	def self.exists id
		return @browser.span(:id => "#{id}").exists?
	end

	def self.getTable xpath
		return @browser.table(:xpath, "#{xpath}")
	end

	def self.takeASnapShoot filename
		@browser.screenshot.save("/Applications/MAMP/htdocs/#{filename}.png")
	end

	def self.getMonthName xpath
		return @browser.b(:xpath, "#{xpath}").text
	end

	def self.getTrsOf table
		return table.trs
	end

	def self.clickOnTd td
		td.click
	end

	def self.cantBook 
		return @browser.text.include?("No room available based on the selected parameters or institution rules.")
	end

	def self.getTheRows
		return @browser.trs(:class => 'ClickableRow')
	end

	def self.nextMonth
		@browser.td(:xpath, "//*[@id='availabilityCalendarTable']/tbody/tr/td[3]").click
	end

	def self.confirmBooking
		@browser.input(:xpath, "/html/body/div[8]/div[3]/input[1]").click
		@browser.input(:xpath, "//*[@id='btnConfirm']").click
		@browser.input(:xpath, "/html/body/div[6]/div[3]/input[1]").click
	end
	
end